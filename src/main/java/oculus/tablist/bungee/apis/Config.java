package oculus.tablist.bungee.apis;

import com.google.common.io.ByteStreams;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import oculus.tablist.bungee.BungeeMain;

import java.io.*;

public class Config {
    private static final File configFile = new File(BungeeMain.getInstance().getDataFolder(), "config.yml");
    private static Configuration c;

    public static void setup() {
        BungeeMain m = BungeeMain.getInstance();
        if (!m.getDataFolder().exists()) m.getDataFolder().mkdir();

        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                try (InputStream is = m.getResourceAsStream("bungee_config.yml");
                     OutputStream os = new FileOutputStream(configFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            c = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void reload() {
        try {
            c = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Configuration Configuration() {
        return c;
    }

}
